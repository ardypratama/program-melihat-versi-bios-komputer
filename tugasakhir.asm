.model small
.code
org 100h
start:
         jmp mulai
nama            db 13,10,'Nama Kamu   : $'
kursi           db 13,10,'No Kursi  : $'
pesanan         db 13,10,'Bungkus/Ditempat  : $'
regis           db 13,10,'Tekan Y untuk registrasi $'
inp_name        db 30,?,30 dup(?)
inp_pesanan     db 30,?,30 dup(?)
inp_kursi       db 13,?,13 dup(?)   
masukkan        db 13,10,'Input Nomor kopi pilihan anda: $'      
list db 13,10,'--------------------------------------'
       db 13,10,'|         MENU KOPI                |'
       db 13,10,'------------------------------------'
       db 13,10,'|NO | LIST         | PRICE         |'
       db 13,10,'------------------------------------'
       db 13,10,'|1  | Americano    | RP.27.500     |'
       db 13,10,'------------------------------------'
       db 13,10,'|2  | Capucino     | RP.23.300     |'
       db 13,10,'------------------------------------'
       db 13,10,'|3  | Coffe Latte  | RP.25.700     |'
       db 13,10,'------------------------------------'
       db 13,10,'|4  | Expresso     | RP.24.300     |'
       db 13,10,'------------------------------------','$'

    mulai:
        mov ah,09h
        lea dx,nama
        int 21h
        mov ah,0ah
        lea dx,inp_name
        int 21h
        push dx  
        
        mov ah,09h
        lea dx,kursi
        int 21h
        mov ah,0ah
        lea dx,inp_kursi
        int 21h
        push dx   
        
        mov ah,09h
        lea dx,pesanan
        int 21h
        mov ah,0ah
        lea dx,inp_pesanan
        int 21h
        push dx

     mov ah,09h
	 mov dx,offset list
	 int 21h
	 mov ah,09h
	 mov dx,offset regis
	 int 21h
	 mov ah,01h
	 int 21h
	 cmp al,'y'
	 jmp proses
	 jne error_msg

error_msg:
        mov ah,09h
        mov dx,offset error_msg
        int 21h
        int 20h

proses:
        mov ah,09h
        mov dx,offset masukkan
        int 21h

        mov ah,01
        int 21h
        mov bh,al   
       
        cmp al,'1'
        je hasil1
        
        cmp al,'2'
        je hasil2
        
        cmp al,'3'
        je hasil3
        
        cmp al,'4'
        je hasil4   
        
hasil1:
	mov ah,09h
	lea dx,teks1
	int 21h
	int 20h

hasil2:
	mov ah,09h
	lea dx,teks2
	int 21h
	int 20h

hasil3:
	mov ah,09h
	lea dx,teks3
	int 21h
	int 20h
	
hasil4:
	mov ah,09h
	lea dx,teks4
	int 21h
	int 20h  
       
;-----------------------------------------------------------------------------------------

teks1  db 13,10,'------------------------------------'
       db 13,10,'AMERICANO Terpilih'
       db 13,10,'Total Price : 27.500 fee 5%'
       db 13,10,'------------------------------------$'

teks2  db 13,10,'------------------------------------'
       db 13,10,'CAPUCINO Terpilih'
       db 13,10,'Total Price : 23.300 fee 5%'
       db 13,10,'------------------------------------$'

teks3  db 13,10,'------------------------------------'
       db 13,10,'COFFEE LATTE Terpilih'
       db 13,10,'Total Price : 25.700 fee 5%'
       db 13,10,'------------------------------------$'

teks4  db 13,10,'------------------------------------'
       db 13,10,'EXPRESSO Terpilih'
       db 13,10,'Total Price : 24.300 fee 5%'
       db 13,10,'------------------------------------$' 
       
;-----------------------------------------------------------------------------------------